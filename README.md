# Instance unique de GNU Emacs

Venant du fond des âges, GNU Emacs n'a pas de mode instance unique comme on peut
l'avoir avec les IDE et éditeurs récents. Si on veut avoir une et une seule
fenêtre GNU Emacs pour toutes ses intéractions, y compris depuis le terminal, il
faut un peu d'adaptation. C'est ce que propose emacs1.

- Un script unique `emacs1` à utiliser en direct, en alias, dans `git/config`.
- Dépends uniquement de bash.
- Indépendant de X11.
- Pas de configuration de GNU Emacs particulière.
- Un fichier `.desktop` fourni avec associations des types MIME pour le
  développement.
- Rebascule le focus sur le terminal qui a lancé la commande (ex: après
  l'édition d'un git commit).

Le processus corresponds à peu près à cela :

- Démarrer GNU Emacs en démon si le serveur Emacs est éteint.
- Ouvrir une fenêtre si aucune fenêtre n'est ouverte.
- Basculer le focus sur la fenêtre Emacs.
- Si un fichier est passé en paramètre, exécuter le client Emacs et rebasculer
  sur la fenêtre du terminal à la fin de l'édition.


``` console
$ emacs1
$ echo $EDITOR
emacs1
$ alias
alias emacs='emacs1'
$
```

Le script `emacs1` est une encapsulation de `emacsclient`. Il accepte les mêmes
options.


## Installation

`emacs1` dépend de GNU Emacs et de bash.

Le projet **est** un role *Ansible*. Le playbook `install.yml` déploie le projet
dans le dossier utilisateur : script et `.desktop` dans `~/.local/`. Le playbook
installe les paquets debian `emacs-gtk`.

``` console
$ ansible-playbook --ask-become-pass -i localhost, install.yml

PLAY [localhost] ****************************************************************
…
PLAY RECAP **********************************************************************
localhost                  : ok=4    changed=0    unreachable=0    failed=0

$
```

Le rôle accepte quelques variables de configuration documentées dans
`defaults/main.yml`.
